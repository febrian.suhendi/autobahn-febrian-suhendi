*** Settings ***
Library  SeleniumLibrary
Library  FakerLibrary
Resource  Common.robot

*** Variables ***
${input_email}  //input[@name='email']
${input_password}  //input[@name='password']
${input_firstName}  //input[@name='first-name']
${input_lastName}  //input[@name='last-name']
${dropdown_industry}  //div[@name='industry']
${input_phoneNumber}  //input[@name='phone-number']
${paragraph_registrationInstruction}  //div[@class='body']//p[@id='instruction']
${list_indonesiaCountryCode}  //li[@data-country-code='id']
${div_phoneFlagContainer}  //div[@class='iti__flag-container']

${phoneNumber}  081122334455

*** Keywords ***
Signup User
    ${email}=  FakerLibrary.Email  True  eurokool.com
    ${password}=  FakerLibrary.Password
    ${firstName}=  FakerLibrary.First Name
    ${lastName}=  FakerLibrary.Last Name
    
    ${button_signup}=  Common.Get Anchor By Text  Sign up
    ${button_submitPersonalInfo}=  Common.Get Anchor By Text  Start using Autobahn
    ${button_joinExistingAccount}=  Common.Get Anchor By Text  Join existing account
    ${industry_option}=  Common.Get Div Element By Id  item-14

    # input email and password
    Wait Until Element Is Visible  ${input_email}  30
    Input Text  ${input_email}  ${email}
    Input Text  ${input_password}  ${password}
    Wait Until Element Is Enabled  ${button_signup}  5
    Click Element  ${button_signup}
    Wait Until Element Is Visible  ${input_firstName}  30
    Element Should Be Visible  ${input_firstName}
    # input personal information
    Input Text  ${input_firstName}  ${firstName}
    Input Text  ${input_lastName}  ${lastName}
    # select industry
    Click Element  ${dropdown_industry}
    Wait Until Element Is Visible  ${industry_option}  3
    Click Element  ${industry_option}
    # input phone number
    Click Element  ${div_phoneFlagContainer}
    Wait Until Element Is Visible  ${list_indonesiaCountryCode}  3
    Click Element  ${list_indonesiaCountryCode}
    Wait Until Element Is Not Visible  ${list_indonesiaCountryCode}  3
    Input Text  ${input_phoneNumber}  ${phoneNumber}
    # submit form
    Wait Until Element Is Enabled  ${button_submitPersonalInfo}  5
    Click Element  ${button_submitPersonalInfo}
    Wait Until Element Is Visible  ${button_joinExistingAccount}  30
    Element Should Be Visible  ${button_joinExistingAccount}
    # join existing account
    Click Element  ${button_joinExistingAccount}  
    # verify registration form submited
    Wait Until Element Is Visible  ${paragraph_registrationInstruction}  60
    Element Should Be Visible  ${paragraph_registrationInstruction}