*** Settings ***
Library  SeleniumLibrary

*** Variables ***
${base_url}  https://autobahn.security/

*** Keywords ***
Get Anchor By Text
    [Arguments]  ${text}
    ${obj}=  Set Variable  //a[contains(., '${text}')]
    RETURN  ${obj}

Get Div Element By Id
    [Arguments]  ${id}
    ${obj}=  Set Variable  //div[@id='${id}']
    RETURN  ${obj}

Navigate To Registration Page
    Open Browser  ${base_url}signup  Chrome
    Maximize Browser Window
    Wait Until Location Is  ${base_url}signup  30
    Location Should Be  ${base_url}signup
