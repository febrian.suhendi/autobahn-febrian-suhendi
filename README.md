# How To Run Test Scripts

Hi, My name is Febrian, I'm using Robot Framework for this project.

## Prerequisites
* Install Python 3.x
* Install Robot Framework
```bash
pip3 install robotframework
```
* Install Selenium library
```bash
pip3 install robotframework-seleniumlibrary
```
* Install Faker library
```bash
pip3 install robotframework-faker
```


## Project Structure
```bash
.
├── Resources                   # test steps scripts
├── Tests                       # Test Suites
└── README.md
```


  
## Run Test Scripts
* On terminal, navigate to project workspace
* Type this command
```bash
robot -d [folder_name_for_test_results] [path_to_test_script_in_Tests_folder]
```
Example :
```bash
robot -d test-result .\Tests
```

## Test Result
Test result will automatically generated in test result folder that you define in running command. There will be 3 main file that automatically generated ;
* log.html --> This file contain detail test steps from the last test run. Open this file on web browser for the better view
* output.xml --> This file contain raw data from the last test run
* report.html --> This file contain general report from the last test run